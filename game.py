from exceptions import *
from player import *
from board import *
from button import *
from bar import *
from menu import *
from rules import *
from rulesDiagonalOnly import *
from rulesStraightOnly import *
from datetime import datetime  
from datetime import timedelta  
import pygame as pygame
from pygame.locals import *
import sys

class Game:
    def __init__(self):
        self.__running = True
        self.__display_surf = None
        self.__image_surf = None
        self.__windowWidth = 896
        self.__windowHeight = 896

        #Obiekty gry
        self.__board = Board()
        self.__player = Player(255, 0, 0)
        self.__player2 = Player(255, 255, 0)
        self.__activePlayer = None

        #Stany gry
        self.__gameStarted = False
        self.__finished = False
        self.__drawed = False

        #Flagi błędów
        self.__NotSelectedRulesErrorFlag = False
        self.__NotSelectedRulesErrorTime = None
        self.__FullColumnErrorFlag = False

        #Reguły gry
        self.__basicRules = True
        self.__diagonalOnlyRules = False
        self.__straightOnlyRules = False

        #Menu
        self.__inMenu = True
        self.__menu = Menu(self.__windowWidth, self.__windowHeight, self.__basicRules, self.__diagonalOnlyRules, self.__straightOnlyRules, exitMenu = lambda: self.exitMenu(), startNewGame = lambda: self.startNewGame(), exitGame = lambda: self.exitGame())
        
        #Początek obszaru rysowania pól gry
        self.__startWidth = 0
        self.__startHeight = 128
        #Rozmiar pola
        self.__fieldSize = 128

        #Wymiary przycisków
        buttonWidth = self.__fieldSize
        buttonHeight = 41
        buttonStartWidth = self.__startWidth
        buttonStartHeight = self.__startHeight - 42
        
        self.__buttonList = [Button(buttonStartWidth + i * buttonWidth, buttonStartHeight, buttonWidth, buttonHeight, onClickFunction = lambda col, player: self.__board.addToken(col, self.__activePlayer)) for i in range(0, 7) ]

        #Menubar
        startBarWidth = 0
        startBarHeight = 0
        barWidth = self.__fieldSize * 7
        barHeight = self.__fieldSize - 41
        
        self.__bar = Bar(startBarWidth, startBarHeight, barWidth, barHeight, (255, 255, 255), enterMenu = lambda: self.enterMenu())
        

    def onInit(self):
        pygame.init()
        self.__display_surf = pygame.display.set_mode((self.__windowWidth, self.__windowHeight), pygame.HWSURFACE)
        self.__running = True
        self.__image_surf = pygame.image.load("fields128.png").convert()

    def changePlayer(self):
        if self.__activePlayer.getColor() == (255, 0, 0):
            self.__activePlayer = self.__player2
        elif self.__activePlayer.getColor() == (255, 255, 0):
            self.__activePlayer = self.__player

    def onEvent(self):
        ev = pygame.event.get()
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
                
        for event in ev:
            if event.type == pygame.QUIT:
                self.__running = False
            elif self.__inMenu == True:
                if event.type == pygame.MOUSEBUTTONDOWN:
                    for button in self.__menu.getAllButtons():
                        if button.isOver(mouse):
                            button.setPressDown()
                        else:
                            button.unpressDown()
                    for checkButton in self.__menu.getRulesSelectList():
                        if checkButton.isOver(mouse):
                            checkButton.setPressDown()
                        else:
                            checkButton.unpressDown()
                if event.type == pygame.MOUSEBUTTONUP:
                    for button in self.__menu.getAllButtons():
                        try:
                            if button.isOver(mouse):
                                button.onClick()
                        except NotSelectedRulesError:
                            self.__NotSelectedRulesErrorFlag = True
                            self.__NotSelectedRulesErrorTime = datetime.now()
                    for checkButton in self.__menu.getRulesSelectList():
                        if checkButton.isOver(mouse):
                            checkButton.onClick(self.__menu.getRulesSelectList().index(checkButton))
            else:
                if event.type == pygame.MOUSEBUTTONDOWN:
                    if self.__bar.getMenuButton().isOver(mouse):
                        self.__bar.getMenuButton().setPressDown()
                    else:
                        self.__bar.getMenuButton().unpressDown()
                    for button in self.__buttonList:
                        if button.isOver(mouse):
                            button.setPressDown()
                        else:
                            button.unpressDown()
                if event.type == pygame.MOUSEBUTTONUP:
                    if self.__bar.getMenuButton().isOver(mouse):
                        self.__bar.getMenuButton().onClick()
                    if not self.__finished:
                        for button in self.__buttonList:
                            if button.isOver(mouse):   
                                try:
                                    col = self.__buttonList.index(button)
                                    row = button.onClick(col, self.__activePlayer.getColor())
                                    if row == None:
                                        raise NotInsertedToken
                                    if self.__gameRules.checkWin(row, col, self.__activePlayer, self.__board) == True:
                                        self.__finished = True
                                        self.__FullColumnErrorFlag = False
                                        return
                                    elif row == 0:
                                        if self.__gameRules.checkDraw(self.__board) == True:
                                            self.__finished = True
                                            self.__drawed = True
                                            self.__FullColumnErrorFlag = False
                                            return
                                    self.changePlayer()
                                except FullColumnError:
                                    self.__FullColumnErrorFlag = True
                                    #print("Ta kolumna jest pełna!\nWybierz inną. \n")
                                    continue
                                except ValueError:
                                    #print("Podano niewłaściwą wartość!")
                                    continue
                                except NotInsertedToken:
                                    #print("Button pressed up != Button pressed down !")
                                    continue
                                else:
                                    self.__FullColumnErrorFlag = False
            

    def startNewGame(self):
        self.__basicRules, self.__straightOnlyRules, self.__diagonalOnlyRules = self.__menu.getRules()
        
        if self.__basicRules == True:
            self.__gameRules = Rules()
        elif self.__straightOnlyRules == True:
            self.__gameRules = RulesStraightOnly()
        elif self.__diagonalOnlyRules == True:
            self.__gameRules = RulesDiagonalOnly()
        else:
            raise NotSelectedRulesError()

        self.__board.resetBoard()
        self.__player.resetNumberOfTokens()
        self.__player2.resetNumberOfTokens()
        if self.__activePlayer.getColor() == (255, 255, 0):
            self.changePlayer()
        self.__finished = False
        self.__drawed = False
        self.__gameStarted = True
        self.__FullColumnErrorFlag = False

        self.exitMenu()
        

    def enterMenu(self):
        self.__menu.onOpenMenu()
        self.__inMenu = True
        

    def exitMenu(self):
        self.__NotSelectedRulesErrorFlag = False
        self.__NotSelectedRulesErrorTime = None
        if (self.__gameStarted == True):
            self.__inMenu = False
    
    def exitGame(self):
        self.__running = False
    
    def onRender(self):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()

        if self.__inMenu == True:
            self.__menu.draw(self.__display_surf, self.__gameStarted, self.__NotSelectedRulesErrorFlag, self.__NotSelectedRulesErrorTime)
        else:
            self.__bar.draw(self.__display_surf, self.__activePlayer, self.__FullColumnErrorFlag, self.__finished, self.__drawed)
            
            #Rysowanie przycisków do wrzucania monet
            for button in self.__buttonList:
                
                button.setAllColors(self.__activePlayer.getColor(), self.__activePlayer.getDarkColor(), (100, 255, 0))
                button.draw(self.__display_surf, text = "Wrzuć monetę", frameBorder = 1)

            #Rysowanie pól
            for i in range(0, 7):
                for j in range(0, 6):
                    if self.__board.getToken(j, i) == 0:
                        self.__display_surf.blit(self.__image_surf, (self.__startWidth + i * self.__fieldSize, self.__startHeight - 1 + j * self.__fieldSize), pygame.Rect(0, 0, self.__fieldSize, self.__fieldSize))
                    elif self.__board.getToken(j, i) == (255, 0, 0):
                        self.__display_surf.blit(self.__image_surf, (self.__startWidth + i * self.__fieldSize, self.__startHeight - 1 + j * self.__fieldSize), pygame.Rect(self.__fieldSize, 0, self.__fieldSize, self.__fieldSize))
                    else:
                        self.__display_surf.blit(self.__image_surf, (self.__startWidth + i * self.__fieldSize, self.__startHeight - 1 + j * self.__fieldSize), pygame.Rect(self.__fieldSize * 2, 0, self.__fieldSize, self.__fieldSize))

        pygame.display.flip()

    def onCleanup(self):
        pygame.quit()

    def onExecute(self):
        if self.onInit() == False:
            self.__running = False

        clock = pygame.time.Clock()
        self.__activePlayer = self.__player
        
        while self.__running :
            self.onEvent()
            self.onRender()
            
            clock.tick(60)
            
        self.onCleanup()

    
if __name__ == "__main__":
    g = Game()
    g.onExecute()
