import pygame
from rectangle import *

class Button(Rectangle):
    def __init__(self, x, y, dx, dy, onClickFunction = lambda : doNothing(), basicColor = (177, 177, 177), onHoverColor = (177, 177, 177), onClickColor = (177, 177, 177), text = "", borderColor = (0, 0, 0)):
        super().__init__(x, y, dx, dy)
        self.__onClickFunction = onClickFunction
        self.__pressedDown = False
        self.__basicColor = basicColor
        self.__onHoverColor = onHoverColor
        self.__onClickColor = onClickColor
        self.__text = text
        self.__borderColor = borderColor

    def draw(self, surface, fontSize = 16, fontColor = (0, 0, 0), text = "", frameBorder = 2):
        pygame.draw.rect(surface, self.__borderColor, (self.getX(), self.getY(), self.getDX(), self.getDY()), 0)

        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()

        if frameBorder == 1:
            if self.isOver(mouse) and click == (1, 0, 0):
                pygame.draw.rect(surface, self.getOnClickColor(), (self.getX() + 1, self.getY() + 1, self.getDX() - 1, self.getDY() - 2), 0)
            elif self.isOver(mouse) and click == (0, 0, 0):
                pygame.draw.rect(surface, self.getOnHoverColor(), (self.getX() + 1, self.getY() + 1, self.getDX() - 1, self.getDY() - 2), 0)
            else:
                pygame.draw.rect(surface, self.getBasicColor(), (self.getX() + 1, self.getY() + 1, self.getDX() - 1, self.getDY() - 2), 0)
        else:
            if self.isOver(mouse) and click == (1, 0, 0):
                pygame.draw.rect(surface, self.getOnClickColor(), (self.getX() + 1, self.getY() + 1, self.getDX() - 2, self.getDY() - 2), 0)
            elif self.isOver(mouse) and click == (0, 0, 0):
                pygame.draw.rect(surface, self.getOnHoverColor(), (self.getX() + 1, self.getY() + 1, self.getDX() - 2, self.getDY() - 2), 0)
            else:
                pygame.draw.rect(surface, self.getBasicColor(), (self.getX() + 1, self.getY() + 1, self.getDX() - 2, self.getDY() - 2), 0)

        #Wyświetlanie tekstu na przycisku
        if self.__text != "":
            font = pygame.font.SysFont('calibri', fontSize)
            displayText = font.render(self.__text, 1, fontColor)
            surface.blit(displayText, (self.getX() + (self.getDX()/2 - displayText.get_width()/2), (self.getY()+ (self.getDY()/2 - displayText.get_height()/2))))
        else:
            font = pygame.font.SysFont('calibri', fontSize)
            displayText = font.render(text, 1, fontColor)
            surface.blit(displayText, (self.getX() + (self.getDX()/2 - displayText.get_width()/2), (self.getY()+ (self.getDY()/2 - displayText.get_height()/2))))
            
    def isOver(self, pos):
        if self.getX() < pos[0] < self.getX() + self.getDX():
            if self.getY() < pos[1] < self.getY() + self.getDY():
                return True
        return False

    def getPressDown(self):
        return self.__pressedDown

    def setPressDown(self):
        self.__pressedDown = True

    def unpressDown(self):
        self.__pressedDown = False
        
    def onClick(self, *args):
        if self.__pressedDown == True:
            return self.__onClickFunction(*args)
                    
    def doNothing(self):
        pass

    def setBasicColor(self, basicColor):
        self.__basicColor = basicColor

    def setOnHoverColor(self, onHoverColor):
        self.__onHoverColor = onHoverColor

    def setOnClickColor(self, onClickColor):
        self.__onClickColor = onClickColor

    def getBasicColor(self):
        return self.__basicColor

    def getOnHoverColor(self):
        return self.__onHoverColor

    def getOnClickColor(self):
        return self.__onClickColor

    def setAllColors(self, basicColor, onHoverColor, onClickColor, borderColor = (0, 0, 0)):
        self.__basicColor = basicColor
        self.__onHoverColor = onHoverColor
        self.__onClickColor = onClickColor
        self.__borderColor = borderColor

    def setBorderColor(self, borderColor):
        self.__borderColor = borderColor
