import pygame
import sys
from pygame.locals import *
from rectangle import *
from button import *

class Bar(Rectangle):
    def __init__(self, x, y, dx, dy, color, enterMenu):
        super().__init__(x, y, dx, dy)
        self.__color = color

        #Przycisk "Menu"
        menuButtonStartWidth = 4 * 128
        menuButtonWidth = 3 * 128
        menuButtonStartHeight = y
        menuButtonHeight = dy
        self.__menuButton = Button(menuButtonStartWidth, menuButtonStartHeight, menuButtonWidth, menuButtonHeight, onClickFunction = enterMenu, basicColor = (100, 100, 100), onHoverColor = (180, 180, 180), onClickColor = (220, 220, 220), text = "Menu")

        #Obszar informacyjny
        self.__turnInfoStartWidth = 0
        self.__turnInfoWidth = menuButtonStartWidth
        self.__turnInfoStartHeight = y
        self.__turnInfoHeight = dy
        self.__turnInfo = Button(self.__turnInfoStartWidth, self.__turnInfoStartHeight, self.__turnInfoWidth, self.__turnInfoHeight)

        #Obszar informacyjny+
        self.__FullColumnErrorInfo = Button(self.__turnInfoWidth * 2 / 5, self.__turnInfoHeight * 3 / 4, self.__turnInfoWidth / 4, self.__turnInfoHeight / 4)
        self.__numberOfTokensInfo = Button(self.__turnInfoWidth * 2 / 5, self.__turnInfoHeight * 3 / 4, self.__turnInfoWidth / 4, self.__turnInfoHeight / 4)
        
    def draw(self, surface, activePlayer, FullColumnErrorFlag, finished = False, draw = False):
        pygame.draw.rect(surface, self.__color, (self.getX(), self.getY(), self.getDX(), self.getDY()), 0)

        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        
        #Rysowanie przycisku "Menu"
        self.__menuButton.draw(surface, fontSize = 48, frameBorder = 1)

        infoColor = activePlayer.getColor()

        #Rysowanie obszaru informacyjnego
        if draw == True:
            infoColor = (200, 200, 200)
            self.__infoText = "Remis"
        elif finished == True:
            self.__numberOfTokensInfoText = "Liczba ruchów: " + (str)(activePlayer.getNumberOfTokens())
            if infoColor == (255, 0, 0):
                self.__infoText = "Wygrał gracz 1"
            else:
                self.__infoText = "Wygrał gracz 2"
        elif infoColor == (255, 0, 0):
            self.__infoText = "Tura gracza 1"
        else:
            self.__infoText = "Tura gracza 2"

        self.__turnInfo.setAllColors(infoColor, infoColor, infoColor)
        self.__turnInfo.draw(surface, fontSize = 42, text = self.__infoText, frameBorder = 1)

        #Rysowanie obszaru informacyjnego+ 
        if FullColumnErrorFlag == True:
            self.__FullColumnErrorInfo.setAllColors(infoColor, infoColor, infoColor, infoColor)
            self.__FullColumnErrorInfo.draw(surface, fontSize = 18, text = "Ta kolumna jest pełna! Wybierz inną.", frameBorder = 1)
        elif finished == True and draw == False:
            self.__numberOfTokensInfo.setAllColors(infoColor, infoColor, infoColor, infoColor)
            self.__numberOfTokensInfo.draw(surface, fontSize = 18, text = self.__numberOfTokensInfoText, frameBorder = 1)
            
            

    def getMenuButton(self):
        return self.__menuButton

        
