import pygame

class Player:
    def __init__(self, r, g, b):
        self.__numberOfTokens = 0
        self.__color = (r, g, b)
        self.__darkColor = (r * 3/4, g *3/4, b * 3/4)

    def getColor(self):
        return self.__color

    def getDarkColor(self):
        return self.__darkColor

    def incrementNumberOfTokens(self):
        self.__numberOfTokens += 1

    def getNumberOfTokens(self):
        return self.__numberOfTokens

    def resetNumberOfTokens(self):
        self.__numberOfTokens = 0
