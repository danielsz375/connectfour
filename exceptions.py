class FullColumnError(Exception):
    pass

class NotSelectedRulesError(Exception):
    pass

class NotInsertedToken(Exception):
    pass
