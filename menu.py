from exceptions import *
from player import *
from board import *
from button import *
from bar import *
from datetime import datetime  
from datetime import timedelta  
import pygame as pygame
from pygame.locals import *
import sys

class Menu:
    def __init__(self, windowWidth, windowHeight, basicRules, diagonalOnlyRules, straightOnlyRules, exitMenu, startNewGame, exitGame):
        
        self.__background = Rectangle(0, 0, windowWidth, windowHeight)
        self.__menuText = Button(windowWidth // 4, 50, windowWidth // 2, 80, basicColor = (125, 140, 220), onHoverColor = (125, 140, 220), onClickColor = (125, 140, 220), text = "Menu główne")

        #Przycisk "Rozpocznij nową grę"
        self.__startNewGameButton = Button(windowWidth // 4, 150, windowWidth // 2, 100, onClickFunction = startNewGame, basicColor = (110, 225, 165), onHoverColor = (115, 240, 200), onClickColor = (145, 255, 210), text = "Rozpocznij nową grę")
        self.__startNewGameErrorInfo = Button(windowWidth // 3, 220, windowWidth // 3, 24, basicColor = (245, 190, 130), onHoverColor = (245, 190, 130), onClickColor = (245, 190, 130), text = "Błąd: Nie wybrano reguł gry!")

        #Reguły gry, rozwijalna lista wyboru
        self.__rulesList = [basicRules, straightOnlyRules, diagonalOnlyRules]

        self.__selectRulesText = Button(windowWidth * 29 // 100, 260, windowWidth * 3 // 7, 71, basicColor = (125, 140, 220), onHoverColor = (125, 140, 220), onClickColor = (125, 140, 220), text = "Wybierz reguły nowej gry:")
        self.__dropDownMenuStatus = False
        self.__dropDownMenuButton = Button(windowWidth * 29 // 100, 330, windowWidth * 3 // 7, 71, onClickFunction = lambda: self.switchDropDownMenu(), basicColor = (110, 225, 165), onHoverColor = (115, 240, 200), onClickColor = (145, 255, 210))
        self.__rulesSelectList = [Button(windowWidth * 29 // 100, 330 + 70 * (i + 1), windowWidth * 3 // 7, 71 , onClickFunction = lambda n: self.setRules(n) ) for i in range(0, 3) ]
        self.__ruleNames = ["Pionowo, poziomo i ukośnie", "Pionowo i poziomo", "Ukośnie"]

        #Przyciski powrotu i wyjścia
        self.__returnButton = Button(windowWidth // 4, 620, windowWidth // 2, 100, onClickFunction = exitMenu, basicColor = (110, 225, 165), onHoverColor = (115, 240, 200), onClickColor = (145, 255, 210), text = "Wróć")
        self.__exitButton = Button(windowWidth // 4, 730, windowWidth // 2, 100, onClickFunction = exitGame, basicColor = (110, 225, 165), onHoverColor = (115, 240, 200), onClickColor = (145, 255, 210), text = "Wyjdź z gry")
        
        self.__buttons = [self.__startNewGameButton, self.__dropDownMenuButton, self.__returnButton, self.__exitButton]
        

    def draw(self, surface, gameStarted, NotSelectedRulesErrorFlag, NotSelectedRulesErrorTime, color = (200, 200, 200)):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
    
        self.__background.draw(surface, (50, 70, 200))
        self.__menuText.draw(surface, fontSize = 48, text = "Menu Główne")

        
        #Rysowanie przycisku nowej gry
        if NotSelectedRulesErrorFlag == True and datetime.now() < NotSelectedRulesErrorTime + timedelta(seconds=3) and self.getRules() == [False, False, False]:
            self.__startNewGameButton.setAllColors((215, 170, 110), (235, 180, 120), (235, 180, 120))
            self.__startNewGameButton.draw(surface, fontSize = 36)
            if self.__startNewGameButton.isOver(mouse):
                self.__startNewGameErrorInfo.setAllColors((235, 180, 120), (235, 180, 120), (235, 180, 120), (235, 180, 120))
            else:
                self.__startNewGameErrorInfo.setAllColors((215, 170, 110), (215, 170, 110), (215, 170, 110), (215, 170, 110))
                
            self.__startNewGameErrorInfo.draw(surface, fontSize = 20, fontColor = (255, 0, 80))
        else:
            self.__startNewGameButton.setAllColors((110, 225, 165), (115, 240, 200), (145, 255, 210))
            self.__startNewGameButton.draw(surface, fontSize = 36)

        self.__selectRulesText.draw(surface, fontSize = 32)
        

        
        #Rysowanie rozwijalnej listy wyboru
        if self.getRules() == [False, False, False]:
            self.__dropDownMenuButton.setAllColors((220, 45, 85), (250, 55, 95), (250, 55, 95))
            self.__dropDownMenuButton.draw(surface, fontSize = 32, text = "Nie wybrano")  
        for ruleOption, ruleName, ruleStatus in zip(self.__rulesSelectList, self.__ruleNames, self.__rulesList):
            if ruleStatus == True:
                self.__dropDownMenuButton.setAllColors((110, 225, 165), (115, 240, 200), (115, 240, 200))
                self.__dropDownMenuButton.draw(surface, fontSize = 32, text = ruleName)
        if self.__dropDownMenuStatus == True:
            self.drawRulesList(surface)

                
        #Rysowanie przycisku powrotu do gry
        if gameStarted == True:
            self.__returnButton.draw(surface, fontSize = 32)

        #Rysowanie przycisku wyjścia z gry
        self.__exitButton.draw(surface, fontSize = 32)


    def getAllButtons(self):
        return self.__buttons

    def onOpenMenu(self):
        self.__rulesList = [False, False, False]
        self.__dropDownMenuStatus = False
        
    def getRulesSelectList(self):
        if self.getDropDownMenuStatus() == True:
            return self.__rulesSelectList
        else:
            return []
    
    def setRules(self, i):
        for j in range(0, 3):
            if j == i:
                self.__rulesList[j] = True
            else:
                self.__rulesList[j] = False
        self.switchDropDownMenu()

    def getRules(self):
        return self.__rulesList


    def switchDropDownMenu(self):
        if self.__dropDownMenuStatus == False:
            self.__dropDownMenuStatus = True
        elif self.__dropDownMenuStatus == True:
            self.__dropDownMenuStatus = False

    def drawRulesList(self, surface):
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        
        for ruleOption, ruleName, ruleStatus in zip(self.__rulesSelectList, self.__ruleNames, self.__rulesList):
            if ruleStatus == False:
                ruleOption.setAllColors((100, 100, 100), (220, 220, 220), (220, 220, 220))
                ruleOption.draw(surface, fontSize = 32, text = ruleName)
            else:
                ruleOption.setAllColors((110, 225, 165), (115, 240, 200), (115, 240, 200))
                ruleOption.draw(surface, fontSize = 32, text = ruleName)

    def getDropDownMenuStatus(self):
        return self.__dropDownMenuStatus

    def closeDropDownMenu(self):
        self.__dropDownMenuStatus = False
