from exceptions import *

class Board:
    def __init__(self):
        self.__numberOfTokens = 0
        self.__rows = 6
        self.__columns = 7
        self.__arr = [[0 for y in range(self.__columns)] for x in range(self.__rows)]                

    def printBoard(self):
    #Wypisanie zawartości tablicy w konsoli
        for row in self.__arr:
            for el in row:
                print("\t|", el, end=' ')
            print("\n")

    def incrementNoT(func):
    #Zliczanie liczby wrzuconych monet przez każdego gracza
        def wrapper(*args):
            row = func(*args)
            args[2].incrementNumberOfTokens()
            return row
        return wrapper

    @incrementNoT        
    def addToken(self, column, activePlayer):
        for row in range(self.__rows - 1, -1, -1):
            if self.__arr[row][column] == 0:
                self.__arr[row][column] = activePlayer.getColor()
                return row
        raise FullColumnError

    def getNumberOfTokens(self):
        return self.__numberOfTokens

    def resetBoard(self):
        self.__init__()

    def getToken(self, row, column):
        return self.__arr[row][column]
    
    def getArr(self):
        return self.__arr

    def getRows(self):
        return self.__rows

    def getColumns(self):
        return self.__columns


