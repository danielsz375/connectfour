import pygame

class Rectangle:
    def __init__(self, x, y, dx, dy):
        self.__x = x
        self.__y = y
        self.__dx = dx
        self.__dy = dy

    def draw(self, surface, color):
        pygame.draw.rect(surface, color, (self.__x, self.__y, self.__dx, self.__dy), 0)

    def getX(self):
        return self.__x
        
    def getDX(self):
        return self.__dx

    def getY(self):
        return self.__y

    def getDY(self):
        return self.__dy
