import pygame as pygame
from pygame.locals import *
import sys

class Rules():
    def __init__(self):
        pass

    def checkWin(self, row, column, activePlayer, board):
        win = False
        if self.checkWinRow(row, column, activePlayer, board) == True:
            win = True
        if self.checkWinColumn(row, column, activePlayer, board) == True:
            win = True
        if self.checkWinDiagonal(row, column, activePlayer, board) == True:
            win = True

        if win == True:
            return True

    def checkWinRow(self, row, column, activePlayer, board):
        sum = 1
        for j in range (column + 1, board.getColumns()):
            if board.getToken(row, j) == activePlayer.getColor():
                sum = sum + 1
            else:
                break
        for j in range (column - 1, -1, -1):
            if board.getToken(row, j) == activePlayer.getColor():
                sum = sum + 1
            else:
                break

        if sum >= 4:
            return True
        else:
            return False

    def checkWinColumn(self, row, column, activePlayer, board):
        sum = 1
        for i in range (row + 1, board.getRows()):
            if board.getToken(i, column) == activePlayer.getColor():
                sum = sum + 1
            else:
                break
        for i in range (row - 1, -1, -1):
            if board.getToken(i, column) == activePlayer.getColor():
                sum = sum + 1
            else:
                break

        if sum >= 4:
            return True
        else:
            return False

    def checkWinDiagonal(self, row, column, activePlayer, board):
        sum = 1   
        for i, j in zip(range(row + 1, board.getRows()), range(column + 1, board.getColumns())):
            if board.getToken(i, j) == activePlayer.getColor():
                sum = sum + 1
            else:
                break
        for i, j in zip(range(row - 1, -1, -1), range(column - 1, -1, -1)):
            if board.getToken(i, j) == activePlayer.getColor():
                sum = sum + 1
            else:
                break

        if sum >= 4:
            return True
        
        
        sum = 1
        for i, j in zip(range(row - 1, -1 , -1), range(column + 1, board.getColumns())):
            if board.getToken(i, j) == activePlayer.getColor():
                sum = sum + 1
            else:
                break
        for i, j in zip(range(row + 1, board.getRows()), range(column - 1, -1, -1)):
            if board.getToken(i, j) == activePlayer.getColor():
                sum = sum + 1
            else:
                break

        if sum >= 4:
            return True
        else:
            return False
    
           
    def checkDraw(self, board):
        for row in board.getArr():
            for el in row:
                if el == 0:
                    return False
        else:
            return True
        
    
